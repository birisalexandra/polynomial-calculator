package Testing;

import static org.junit.Assert.*;

import javax.swing.SingleSelectionModel;

import org.junit.Test;

import Models.IntegerM;
import Models.Polinom;

public class Test2 {

	@Test
	public void test() {
			String s1 = "6x^2+3x^1";
			String s2 = "3x^1";
			Polinom p1 = new Polinom();
			Polinom p2 = new Polinom();
			p1.splitPolinom(s1);
			p2.splitPolinom(s2);
			
			String s3 = "6x^2+6x^1";
			Polinom p3 = new Polinom();
			p3.splitPolinom(s3);
			p3.sort();
		
			Polinom output1 = p1.addition(p2);
		
			assertEquals(p3.printPolinom(), output1.printPolinom());	
	}
	
	@Test
	public void test2(){
		String s1 = "6x^2+3x^1";
		String s2 = "3x^1";
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.splitPolinom(s1);
		p2.splitPolinom(s2);
		
		String s3 = "6x^2+0x^1";
		Polinom p3 = new Polinom();
		p3.splitPolinom(s3);
		p3.sort();
		
		Polinom output1 = p1.substraction(p2);
		assertEquals(p3.printPolinom(), output1.printPolinom());
	}
	
	@Test
	public void test3(){
		String s1 = "6x^2+3x^1";
		String s2 = "3x^1";
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.splitPolinom(s1);
		p2.splitPolinom(s2);
		
		String s3 = "18x^3+9x^2";
		Polinom p3 = new Polinom();
		p3.splitPolinom(s3);
		p3.sort();
		
		Polinom output1 = p1.multiplication(p2);
		assertEquals(p3.printPolinom(), output1.printPolinom());
	}

}
