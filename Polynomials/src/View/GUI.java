package View;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JTextField textField_2 = new JTextField();
	private JPanel panel = new JPanel();
	private JButton btnAddition = new JButton("Addition");
	private JButton btnSubstraction = new JButton("Substraction");
	private final JButton btnMultiplication = new JButton("Multiplication");
	private final JButton btnDivision = new JButton("Division");
	private final JButton btnDerivationP = new JButton("Derivation P1");
	private final JButton btnIntegrationP = new JButton("Integration P1");
	private final JButton btnClear = new JButton("Clear");
	
	public JTextField getTextField() {
		return textField;
	}
	
	public JTextField getTextField1() {
		return textField_1;
	}
	
	public JTextField getTextField2() {
		return textField_2;
	}

	public void setTextField(String text) {
		this.textField_2.setText(text); 
	}
	
	public void setTextField1(String text) {
		this.textField.setText(text); 
	}
	
	public void setTextField2(String text) {
		this.textField_1.setText(text); 
	}
	
	public GUI() {
		initialize();
	}
	
	public void initialize() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblThePolynomialWill = new JLabel("The polynomial will have the form: ax^n+bx^n-1+...");
		lblThePolynomialWill.setBounds(65, 11, 300, 25);
		panel.add(lblThePolynomialWill);
		
		JLabel lblP = new JLabel("P1");
		lblP.setBounds(24, 50, 46, 14);
		panel.add(lblP);
		
		JLabel lblP_1 = new JLabel("P2");
		lblP_1.setBounds(24, 85, 46, 14);
		panel.add(lblP_1);
		
		getTextField().setBounds(48, 47, 196, 20);
		panel.add(getTextField());
		getTextField().setColumns(10);
		
		textField_1.setBounds(48, 82, 196, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblResult = new JLabel("Result");
		lblResult.setBounds(123, 113, 46, 14);
		panel.add(lblResult);
		
		textField_2.setBounds(48, 138, 196, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		btnAddition.setBounds(296, 47, 118, 23);
		panel.add(btnAddition);
	
		btnSubstraction.setBounds(296, 81, 118, 23);
		panel.add(btnSubstraction);
		btnMultiplication.setBounds(296, 113, 118, 23);
		
		panel.add(btnMultiplication);
		btnDivision.setBounds(296, 147, 118, 23);
		
		panel.add(btnDivision);
		btnDerivationP.setBounds(296, 179, 118, 23);
		
		panel.add(btnDerivationP);
		btnIntegrationP.setBounds(296, 213, 118, 23);
		
		panel.add(btnIntegrationP);
		btnClear.setBounds(97, 169, 89, 23);
		
		panel.add(btnClear);
	}
	//adds action listener for each button
	public void addAddListener(ActionListener a) {
		btnAddition.addActionListener(a);
	}
	
	public void addSubListener(ActionListener a) {
		btnSubstraction.addActionListener(a);
	}
	
	public void addMulListener(ActionListener a) {
		btnMultiplication.addActionListener(a);
	}
	
	public void addDivListener(ActionListener a) {
		btnDivision.addActionListener(a);
	}
	
	public void addDerivListener(ActionListener a) {
		btnDerivationP.addActionListener(a);
	}
	
	public void addIntegListener(ActionListener a) {
		btnIntegrationP.addActionListener(a);
	}
	
	public void addClearListener(ActionListener a) {
		btnClear.addActionListener(a);
	}
}
