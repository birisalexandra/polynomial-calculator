package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Models.Polinom;
import View.GUI;

public class Controller {
	private GUI g;
	
	public Controller(GUI g) {
		this.g = g;
		this.g.addAddListener(new Addition());
		this.g.addSubListener(new Substraction());
		this.g.addDerivListener(new Derivation());
		this.g.addIntegListener(new Integration());
		this.g.addMulListener(new Multiplication());
		this.g.addDivListener(new Division());
		this.g.addClearListener(new Clear());
	}
	
	public class Addition implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s1 = g.getTextField().getText();
			String s2 = g.getTextField1().getText();
			Polinom p1 = new Polinom(); 
			Polinom p2 = new Polinom();
			p1.splitPolinom(s1); 
			p2.splitPolinom(s2);
			g.setTextField(p1.addition(p2).printPolinom());
		}
	}
	
	public class Substraction implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s1 = g.getTextField().getText();
			String s2 = g.getTextField1().getText();
			Polinom p1 = new Polinom(); 
			Polinom p2 = new Polinom();
			p1.splitPolinom(s1); 
			p2.splitPolinom(s2);
			g.setTextField(p1.substraction(p2).printPolinom());;
		}
	}
	
	public class Derivation implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s1 = g.getTextField().getText();
			Polinom p1 = new Polinom();
			p1.splitPolinom(s1);
			g.setTextField(p1.derivation().printPolinom());
		}
	}
	
	public class Integration implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s1 = g.getTextField().getText();
			Polinom p1 = new Polinom();
			p1.splitPolinom(s1);
			g.setTextField(p1.integration().printPolinom());
		}
	}
	
	public class Multiplication implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s1 = g.getTextField().getText();
			String s2 = g.getTextField1().getText();
			Polinom p1 = new Polinom(); 
			Polinom p2 = new Polinom();
			p1.splitPolinom(s1); 
			p2.splitPolinom(s2);
			g.setTextField(p1.multiplication(p2).printPolinom());
		}
	}
	
	public class Division implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String s1 = g.getTextField().getText();
			String s2 = g.getTextField1().getText();
			Polinom p1 = new Polinom(); 
			Polinom p2 = new Polinom();
			p1.splitPolinom(s1); 
			p2.splitPolinom(s2);
			g.setTextField(p1.division(p2));
		}
	}
	
	public class Clear implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			g.setTextField("");
			g.setTextField1("");
			g.setTextField2("");
		}
	}
}
