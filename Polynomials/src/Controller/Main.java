package Controller;
import Models.IntegerM;
import Models.Monom;
import Models.Polinom;
import View.GUI;

public class Main {

	public static void main(String[] args) {
		GUI g = new GUI();
		Controller contr = new Controller(g);
		g.setVisible(true);
	}
}
