package Models;

public abstract class Monom {
	protected int grad;
	protected Number coeff; //the coefficient is of type Number because it can be either double or int
	
	public Monom(Number coeff, int grad) {
		this.grad = grad;
		this.coeff = coeff;
	}
	
	public void setGrad(int grad) {
		this.grad = grad;
	}
	
	public int getGrad() {
		return this.grad;
	}
	
	public void setCoeff(Number coeff) {
		this.coeff = coeff;
	}
	
	public Number getCoeff() {
		return this.coeff;
	}
	
	@Override
	public String toString() { //we override the toString method to transform each monomial into a string
		return this.getCoeff() + "x^" + this.getGrad();
	}
	
}
