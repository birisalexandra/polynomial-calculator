package Models;

public class IntegerM extends Monom{
	
	public IntegerM(int coeff, int grad) {
		super(coeff, grad);
	}
	
	public Monom addMonom(Monom m) {
		return new IntegerM(this.getCoeff().intValue() + m.getCoeff().intValue(), this.getGrad());
	}
}

