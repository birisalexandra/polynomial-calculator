package Models;
import java.util.ArrayList;
import java.util.Collections;

public class Polinom {
	private ArrayList<Monom> monoame;
	
	public Polinom() { //constructor used to instantiated a new ArrayList of monomials
		this.monoame = new ArrayList<Monom>();
	}

	public Polinom(Polinom p1) { //constructor used to add a new polynomial into the current ArrayList
		this.monoame = new ArrayList<Monom>();
		for(Monom currentM: p1.getMonoame()) {
			this.getMonoame().add(currentM);
		}
	}
	
	public ArrayList<Monom> getMonoame() {
		return monoame;
	}

	public void splitPolinom(String p) {
		p = p.replaceAll("-", "+-"); //as we will split the polynomial after the sign +, we need to add it before every minus
		String[] monom = p.split("\\+"); //split into monomial after + and toss it
		for(int i = monom.length - 1; i >= 0; i--) {
			String[] caracter = monom[i].split("x" + "\\^"); //split each monomial into coefficient and sign 
			if(caracter[0].contains(".") == true)
				monoame.add(new RealM(Double.parseDouble(caracter[0]), Integer.parseInt(caracter[1])));
			else
				monoame.add(new IntegerM(Integer.parseInt(caracter[0]), Integer.parseInt(caracter[1])));
		}
	}
	
	public String printPolinom() { //transform a polynomial into a string 
		String finalRez = new String();
		for(Monom m: this.getMonoame()) {
			if(m.getCoeff().doubleValue() < 0 || m == this.getMonoame().get(0)) {
				finalRez += m.toString(); //adds to the final string each monomial converted into string  
			}
			else if(m.getCoeff().doubleValue() > 0){
				finalRez += "+" + m.toString();
			}
		}
		return finalRez;
	}
	
	public void addMonom(Monom m) {
		boolean check = false; //this variable checks if a monomial with the same degree exists in out polynomial
		for(Monom mon: this.getMonoame()) {
			if(mon.getGrad() == m.getGrad()) {
				check = true; //if found we just add the coefficient to the existing one
				if(mon instanceof RealM)
					mon.setCoeff(mon.getCoeff().doubleValue() + m.getCoeff().doubleValue());
				else {
					mon.setCoeff(mon.getCoeff().intValue() + m.getCoeff().intValue());
				}
			}
		}
		
		if(check == false) { //if not found we add the entire monomial 
			this.getMonoame().add(m);
		}
	}
	
	public Polinom addition(Polinom p1) { 
		Polinom result = p1; //we save the first polynomial into the final result
		for(Monom m: this.getMonoame()) {
			result.addMonom(m); //we add to the first polynomial the monomials from the second
		}
		result.sort(); //we sort the result so the degrees will be in descending order
		return result;
	}
	
	public Polinom substraction(Polinom p1) {
		Polinom result = this;
		for(Monom m: p1.getMonoame()) {
			if(m instanceof IntegerM) 
				m.setCoeff(m.getCoeff().intValue() * -1); //we invert all the signs from the first polynomial 
			else
				m.setCoeff(m.getCoeff().doubleValue() * -1);
		}
		return result.addition(p1); // p2 - p1 = p2 + p1 inverted
	}
	
	public Polinom multiplication(Polinom p1) {
		Polinom result = new Polinom();
		for(Monom m1: this.getMonoame()) { //for each monomial from the first polynomial we take each monomial from second polynomial
			for(Monom m2: p1.getMonoame()) {
				result.addMonom(new IntegerM(m1.getCoeff().intValue() * m2.getCoeff().intValue(), m1.getGrad() + m2.getGrad()));
			}
		}
		result.sort();
		return result;
	}
	
	public Polinom derivation() {
		Polinom result = this;
		for(Monom m: result.getMonoame()) {
			if(m.getGrad() == '0') //if the degree = 0, means we have a constant so its derivative should be 0
				m.setCoeff(0);
			else {
				m.setCoeff(m.getCoeff().intValue() * m.getGrad());
				m.setGrad(m.getGrad() - 1);
			}
		}	
		result.sort();
		return result;
	}
	
	public Polinom integration() {
		Polinom result = this;
		for(Monom m: result.getMonoame()) {
			m.setGrad(m.getGrad() + 1);
			if(m.getCoeff().intValue() % m.getGrad() == 0)
				m.setCoeff(m.getCoeff().intValue() / m.getGrad());
			else
				m.setCoeff(m.getCoeff().doubleValue() / m.getGrad());
		}
		result.sort();
		return result;
	}
	
	public void multiplyMonom(Monom m) {
		for(Monom mon: this.getMonoame()) {
			if(mon instanceof RealM) {
				mon.setCoeff(mon.getCoeff().doubleValue() * m.getCoeff().doubleValue());
				mon.setGrad(mon.getGrad() + m.getGrad());
			}
			else {
				mon.setCoeff(mon.getCoeff().intValue() * m.getCoeff().intValue());
				mon.setGrad(mon.getGrad() + m.getGrad());
			}
		}
	}
	
	public String division(Polinom p2) {
		Polinom p1 = this;
		p1.sort(); p2.sort();
		Polinom result = new Polinom();
		Monom m1 = p1.getMonoame().get(0);
		Monom m2 = p2.getMonoame().get(0);
		int grad = 0;
		boolean ok = true;
		do{
			ok = true;
			if(m1.getCoeff().doubleValue() % m2.getCoeff().doubleValue()== 0) {
		 		IntegerM m = new IntegerM(m1.getCoeff().intValue() / m2.getCoeff().intValue(), m1.getGrad() - m2.getGrad());
				result.addMonom(m);
				System.out.println(result.printPolinom());
				Polinom aux1 = p2;
				aux1.multiplyMonom(m);
				System.out.println(aux1.printPolinom());
				p1 = p1.substraction(aux1);
				System.out.println(p1.printPolinom());
			}
			else {
				RealM m = new RealM(m1.getCoeff().doubleValue() / m2.getCoeff().doubleValue(), m1.getGrad() - m2.getGrad());
				result.addMonom(m);
				Polinom aux1 = p2;
				aux1.multiplyMonom(m);
				Polinom aux2 = p1.substraction(aux1);
				p1 = aux2;
			}
			for(Monom m: p1.getMonoame()) 
				if(m.getCoeff().doubleValue() != 0) {
					grad = m.getGrad();
					System.out.println(grad);
					m1.setGrad(grad);
					System.out.println(m1.getGrad());
					m1.setCoeff(m.getCoeff());
					System.out.println(m1.getCoeff());
					ok = false;
					break;
				}
			if(ok == true)
				grad = 0;
			System.out.println(p2.getMonoame().get(0).getGrad());
		} while(p2.getMonoame().get(0).getGrad() >= grad);
		return result.printPolinom() + " Rest: " + p1.printPolinom();
	}
	
	public void sort() { //used for sorting the monomials in descending order
		Collections.sort(this.monoame, new MComparator());
	}
}
