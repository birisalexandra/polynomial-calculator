package Models;
public class RealM extends Monom{
	
	public RealM(double coeff, int grad) {
		super(coeff, grad);
	}
	
	public Monom addMonom(Monom m) {
		return new RealM(this.getCoeff().doubleValue() + m.getCoeff().doubleValue(), this.getGrad());
	}
}