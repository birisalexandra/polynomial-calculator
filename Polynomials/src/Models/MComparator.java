package Models;

import java.util.Comparator;

public class MComparator implements Comparator<Monom>{

	@Override
	public int compare(Monom m1, Monom m2) {
		if(m1.getGrad() > m2.getGrad()) {
			return -1;
		}
		else if (m1.getGrad() < m2.getGrad()) {
			return 1;
		}
		else
			return 0;
	}
	

}
